
#include "io.h"
#include "printk.h"
#include "smp.h"
#include "test_app.h"

void delay(volatile uint64_t time)
{
    while(time--);
}


void cpu1_main(void)
{
    write32(0x0300B04C, 0x77177777);
    setbits_le32(0x0300B058, (1 << 13));

    while(1)
    {
        
    }
}

extern volatile uint64_t ttb0_base[512];


int main(void)
{
    test_timer0_init();

    // return_to_fel(fel_stash.sp, fel_stash.lr);

    write32(0x0300B04C, 0x77177777);

    // sunxi_cpu_start(1, cpu1_main);

    write32(0x70000000, 0x12345678);
    printk("system init = 0x%x\r\n", read32(0x70000000));

    while(1)
    {
        setbits_le32(0x0300B058, (1 << 13));
        delay(0x500000);
        clrbits_le32(0x0300B058, (1 << 13));    
        delay(0x500000);
    }
}

