#include "printk.h"
#include "io.h"
#include "sys_irq.h"

static sys_irq_handler_t irq_table[SYS_IRQ_END];

void enable_interrupts(void)
{
    asm volatile("msr daifclr, #2");
}

void disable_interrupts(void)
{
    asm volatile("msr daifset, #2");
}

void sys_irq_register_handler(uint32_t irq, sys_irq_handler_t handler)
{
    irq_table[irq] = handler;
}

void sys_irq_handler(uint32_t irq_num)
{
    uint32_t int_num = irq_num & 0x3ff;

    /* check irq number */
    if(int_num > SYS_IRQ_END)
    {
        return;
    }

    enable_interrupts();

    irq_table[int_num]();

    disable_interrupts();
}

void do_sync(void)
{
    printk("sync error\r\n");

    while(1);
}

void do_irq(void)
{
    uint32_t irq_num = 0;

    irq_num = gic400_get_irq_num();

    sys_irq_handler(irq_num);

    gic400_irq_end(irq_num);
}

