
#include "io.h"
#include "printk.h"

#define GIC_BASE        0x03020000

enum {
	DIST_CTRL 			= 0x1000,
	DIST_CTR 			= 0x1004,
    DIST_GROUP 			= 0x1080,
	DIST_ENABLE_SET 	= 0x1100,
	DIST_ENABLE_CLEAR 	= 0x1180,
	DIST_PENDING_SET 	= 0x1200,
	DIST_PENDING_CLEAR	= 0x1280,
	DIST_ACTIVE_BIT		= 0x1300,
	DIST_PRI			= 0x1400,
	DIST_TARGET			= 0x1800,
	DIST_CONFIG			= 0x1c00,
	DIST_SOFTINT		= 0x1f00,

	CPU_CTRL 			= 0x2000,
	CPU_PRIMASK 		= 0x2004,
	CPU_BINPOINT 		= 0x2008,
	CPU_INTACK 			= 0x200c,
	CPU_EOI 			= 0x2010,
	CPU_RUNNINGPRI 		= 0x2014,
	CPU_HIGHPRI 		= 0x2018,
};

void gic400_irq_enable(int irq)
{
	write32(GIC_BASE + DIST_ENABLE_SET + (irq / 32) * 4, 1 << (irq % 32));
}

void gic400_irq_disable(int irq)
{
	write32(GIC_BASE + DIST_ENABLE_CLEAR + (irq / 32) * 4, 1 << (irq % 32));
}

/*
 * irq: interrupt num 
 * cpumask: (1 << 0) for cpu0, (1 << 1) for cpu1, (1 << 2) for cpu2, (1 << 3) for cpu3 
*/
void gic400_set_cpu(int irq, int cpumask)
{
    uint32_t target = 0;

    target = read32(GIC_BASE + DIST_TARGET + (irq / 4) * 4);
    target &= ~(0x0ff << ((irq % 4)*8));
    target |= (cpumask << ((irq % 4)*8));
    write32(GIC_BASE + DIST_TARGET + (irq / 4) * 4, target);
}

/*
 * 根据 Distributor 和 CPU Interface 的设置，优先级为
 * 0x00 ~ 0xF0（低4位无效），高四位都是抢占优先级，值越
 * 小，优先级越高
 */
void gic400_set_priority(int irq, int priority)
{
    uint32_t mask = 0;

    mask = read32(GIC_BASE + DIST_PRI + (irq / 4) * 4);   
    mask &= ~(0x0ff << ((irq % 4)*8));
    mask |= ((priority & 0xff) << ((irq % 4)*8));
    write32(GIC_BASE + DIST_PRI + (irq / 4) * 4, mask);
}

void gic400_send_sgi(int irq, int cpumask)
{
    write32(GIC_BASE + DIST_SOFTINT, (cpumask << 16) | irq);
}

void gic400_dist_init(void)
{
	uint32_t gic_irqs;
	uint32_t cpumask;
	int i;

	write32(GIC_BASE + DIST_CTRL, 0x0);

	/*
	 * Find out how many interrupts are supported.
	 * The GIC only supports up to 1020 interrupt sources.
	 */
	gic_irqs = read32(GIC_BASE + DIST_CTR) & 0x1f;
	gic_irqs = (gic_irqs + 1) * 32;
	if(gic_irqs > 1020)
		gic_irqs = 1020;

	/*
	 * Set all global interrupts to CPU0 only.
	 */
	cpumask = 1 << 0;
	cpumask |= cpumask << 8U;
	cpumask |= cpumask << 16U;
    cpumask |= cpumask << 24U;
	for(i = 32; i < gic_irqs; i += 4)
		write32(GIC_BASE + DIST_TARGET + (i / 4) * 4, cpumask);

	/*
	 * Set all global interrupts to be level triggered
	 */
	for(i = 32; i < gic_irqs; i += 16)
		write32(GIC_BASE + DIST_CONFIG + (i / 16) * 4, 0);

	/*
	 * Set priority on all global interrupts.
	 */
	for(i = 0; i < gic_irqs; i += 4)
		write32(GIC_BASE + DIST_PRI + (i / 4) * 4, 0xa0a0a0a0);

	/*
	 * Disable all interrupts
	 */
	for(i = 0; i < gic_irqs; i += 32)
		write32(GIC_BASE + DIST_ENABLE_CLEAR + (i / 32) * 4, 0xffffffff);

    /* set gpour0 */
    for(i = 0; i < gic_irqs; i += 32)
    {
        write32(GIC_BASE + DIST_GROUP + (i / 32) * 4, 0);
    }

	write32(GIC_BASE + DIST_CTRL, 0x1);
}

/*
 * 优先级的设置：优先级总共有8位，CPU_PRIMASK设置完后，只有高四位起作用，
 * CPU_BINPOINT设置完后，说明高四位都是抢占优先级，DIST_PRI数值越小，优先级
 * 越高，现在的优先级等级是 0x00 ~ 0xF0（低4位无效）
*/
void gic400_cpu_init(void)
{
    /* 16 个优先级 */
	write32(GIC_BASE + CPU_PRIMASK, 0xf0);

    /* 4 位抢占优先级，4 位子优先级  */
	write32(GIC_BASE + CPU_BINPOINT, 0x3);

	write32(GIC_BASE + CPU_CTRL, 0x1);
}

void gic400_init(void)
{
    gic400_dist_init();
    gic400_cpu_init();
}

uint32_t gic400_get_irq_num(void)
{
    uint32_t temp = 0;
    temp = read32(GIC_BASE + CPU_INTACK);
    temp &= 0x3ff;
    return temp;
}

void gic400_irq_end(uint32_t irq_num)
{
    write32(GIC_BASE + CPU_EOI, irq_num);
}
