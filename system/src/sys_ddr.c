
#include "sys_ddr.h"
#include "printk.h"
#include "sys_i2c.h"
#include "sys_clock.h"

enum 
{
	AXP313A_CHIP_VERSION = 0x3,
	AXP313A_OUTPUT_CTRL1 = 0x10,
	AXP313A_DCDCD_VOLTAGE = 0x15,
	AXP313A_SHUTDOWN = 0x32,
};

#define AXP313A_I2C_ADDR    0x36
#define AXP313A_CHIP_ID     0x4B

static u8 axp_mvolt_to_cfg(int mvolt, int min, int max, int div)
{
	if (mvolt < min)
		mvolt = min;
	else if (mvolt > max)
		mvolt = max;

	return  (mvolt - min) / div;
}

#define AXP313A_DCDC3_1200MV_OFFSET 65
int axp_set_dcdc3(unsigned int mvolt)
{
	uint8_t cfg;
    uint8_t m_data, s_data;

	if (mvolt >= 1220)
	{
		cfg = AXP313A_DCDC3_1200MV_OFFSET + axp_mvolt_to_cfg(mvolt, 1220, 1840, 20);
	}
	else
    {
		cfg = axp_mvolt_to_cfg(mvolt, 500, 1200, 10);
    }

	if (mvolt == 0)
    {
        m_data = AXP313A_OUTPUT_CTRL1;
        s_data = 0x1b;
		sys_i2c_write(AXP313A_I2C_ADDR, &m_data, 1, &s_data, 1);
		// sys_i2c_read(AXP313A_I2C_ADDR, &m_data, 1, &s_data, 1);
        // printk("s_data = 0x%x\r\n", s_data);

        return 0;
    }

    m_data = AXP313A_DCDCD_VOLTAGE;
    s_data = cfg;
    sys_i2c_write(AXP313A_I2C_ADDR, &m_data, 1, &s_data, 1);
 
    m_data = AXP313A_OUTPUT_CTRL1;
    s_data = 0x1f;
    sys_i2c_write(AXP313A_I2C_ADDR, &m_data, 1, &s_data, 1);

    return 0;
}

static int axp_init(void)
{
    uint8_t s_data = AXP313A_CHIP_VERSION;
    uint8_t axp_chip_id;

    sys_i2c_init();

    sys_i2c_read(AXP313A_I2C_ADDR, &s_data, 1, &axp_chip_id, 1);
    
    // printk("axp_chip_id = 0x%x\r\n", axp_chip_id);

    if(axp_chip_id != AXP313A_CHIP_ID)
    {
        return -1;
    }

    axp_set_dcdc3(1500);

    return 0;
}

unsigned long sunxi_dram_init(void);

void sys_ddr_init(void)
{
    int status = 0;

    status = axp_init();

    if(status != 0)
    {
        printk("pmu set failed\r\n");
        return;
    }

    /* ddr init */
    sunxi_dram_init();
}