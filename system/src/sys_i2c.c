#include "sys_i2c.h"
#include "sys_clock.h"
#include "sys_gpio.h"
#include "sys_cpu.h"
#include "printk.h"

static int sys_i2c_stop(void);

void sys_i2c_init(void)
{
	u32_t addr;
	u32_t val;

	clock_twi_onoff(5, 1);
	sunxi_gpio_set_cfgpin(SUNXI_GPL(0), SUN50I_H616_GPL_R_TWI);
	sunxi_gpio_set_cfgpin(SUNXI_GPL(1), SUN50I_H616_GPL_R_TWI);

    /* reset twi */
    addr = SUNXI_R_TWI_BASE + TWI_SRST_OFFSET;
    write32(addr, (1 << 0));
    while(read32(addr) & (1 << 0));

    /* set clock 400Khz */
    addr = SUNXI_R_TWI_BASE + TWI_CCR_OFFSET;
    val = 0x28;
    write32(addr, val);  

    sys_i2c_stop();
}

static int sys_i2c_wait(int expected_status)
{
	int control, status;

    while(1)
    {
        control = read32(SUNXI_R_TWI_BASE + TWI_CNTR_OFFSET);

        if (control & I2C_CONTROL_IFLG)
        {
            status = read32(SUNXI_R_TWI_BASE + TWI_STAT_OFFSET);

            if(status == expected_status)
            {
                return 0;
            }
        }
    }
    
    return -1;
}

static int sys_i2c_stop(void)
{
	int control, stop_status;

    /* Assert STOP */
	control = I2C_CONTROL_TWSIEN | I2C_CONTROL_STOP | I2C_CONTROL_IFLG;
	write32(SUNXI_R_TWI_BASE + TWI_CNTR_OFFSET, control);

    while(1)
    {
		stop_status = read32(SUNXI_R_TWI_BASE + TWI_STAT_OFFSET);
		if (stop_status == I2C_STAT_IDLE)
        {
			break;
        }
    }

    control = read32(SUNXI_R_TWI_BASE + TWI_CNTR_OFFSET);

    return 0;
}

static int sys_i2c_start(int expected_status)
{
    int control;

	/* Assert START */
    control = I2C_CONTROL_TWSIEN | I2C_CONTROL_START | I2C_CONTROL_IFLG;
	write32(SUNXI_R_TWI_BASE + TWI_CNTR_OFFSET, control);

	/* Wait for controller to process START */
    sys_i2c_wait(expected_status);
	return 0;
}

static int sys_i2c_send(uint8_t byte, int expected_status)
{
	/* Write byte to data register for sending */
	write32(SUNXI_R_TWI_BASE + TWI_DATA_OFFSET, byte);

	/* Clear any pending interrupt -- that will cause sending */
	write32(SUNXI_R_TWI_BASE + TWI_CNTR_OFFSET, I2C_CONTROL_TWSIEN | I2C_CONTROL_IFLG);

	/* Wait for controller to receive byte, and check ACK */
	sys_i2c_wait(expected_status);

    return 0;
}

static int sys_i2c_recv(uint8_t *byte, int ack_flag)
{
	int expected_status, control;

	/* Compute expected status based on passed ACK flag */
	expected_status = ack_flag ? I2C_STAT_RXD_ACK : I2C_STAT_RXD_NAK;
	/* Acknowledge *previous state*, and launch receive */
	control = I2C_CONTROL_TWSIEN;
	control |= ack_flag == I2C_READ_ACK ? I2C_CONTROL_ACK : 0;
	write32(SUNXI_R_TWI_BASE + TWI_CNTR_OFFSET, control | I2C_CONTROL_IFLG);
	/* Wait for controller to receive byte, and assert ACK or NAK */
	sys_i2c_wait(expected_status);
	/* If we did receive the expected byte, store it */
	*byte = read32(SUNXI_R_TWI_BASE + TWI_DATA_OFFSET);

	return 0;
}

int sys_i2c_read(uint8_t addr, uint8_t *m_data, int m_len, uint8_t *r_data, int r_len)
{
    int i = 0;
    int expected_start = I2C_STAT_TX_START;

    /* Begin i2c write to send bytes */
    if(m_len > 0)
    {
        /* Send addr byte */
        sys_i2c_start(expected_start);
        sys_i2c_send((addr << 1), I2C_STAT_TX_AW_ACK);

        /* Send cmd bytes */
        while(m_len--)
        {
            sys_i2c_send(m_data[i++], I2C_STAT_TXD_ACK);
        }  

        expected_start = I2C_STAT_TX_RSTART;
        i = 0;
    }

    /* Begin i2c read to receive data bytes */
    sys_i2c_start(expected_start);
    sys_i2c_send((addr << 1) | 1, I2C_STAT_TX_AR_ACK);

    while(r_len--)
    {
        sys_i2c_recv(r_data++, r_len > 0 ? I2C_READ_ACK : I2C_READ_NAK);
    }

    sys_i2c_stop();

    return 0;
}

int sys_i2c_write(uint8_t addr, uint8_t *m_data, int m_len, uint8_t *s_data, int s_len)
{
    int i = 0;

	/* Begin i2c write to send first the address bytes, then the data bytes */
    sys_i2c_start(I2C_STAT_TX_START);
    sys_i2c_send((addr << 1), I2C_STAT_TX_AW_ACK);

    /* Send cmd bytes */
    i = 0;
    while(m_len--)
    {
        sys_i2c_send(m_data[i++], I2C_STAT_TXD_ACK);
    }  
    
    /* Send data bytes */
    i = 0;
    while(s_len--)
    {
        sys_i2c_send(s_data[i++], I2C_STAT_TXD_ACK);
    }

    sys_i2c_stop();

    return 0;
}






