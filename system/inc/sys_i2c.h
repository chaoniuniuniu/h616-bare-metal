
#ifndef _SYS_I2C_H
#define _SYS_I2C_H

#include "io.h"

#define TWI_ADDR_OFFSET     (0x0000)
#define TWI_XADDR_OFFSET    (0x0004)
#define TWI_DATA_OFFSET     (0x0008)
#define TWI_CNTR_OFFSET     (0x000c)
#define TWI_STAT_OFFSET     (0x0010)
#define TWI_CCR_OFFSET      (0x0014)
#define TWI_SRST_OFFSET     (0x0018)
#define TWI_EFR_OFFSET      (0x001c)
#define TWI_LCR_OFFSET      (0x0020)

enum {
	I2C_STAT_BUS_ERROR	= 0x00,
	I2C_STAT_TX_START	= 0x08,
	I2C_STAT_TX_RSTART	= 0x10,
	I2C_STAT_TX_AW_ACK	= 0x18,
	I2C_STAT_TX_AW_NAK	= 0x20,
	I2C_STAT_TXD_ACK	= 0x28,
	I2C_STAT_TXD_NAK	= 0x30,
	I2C_STAT_LOST_ARB	= 0x38,
	I2C_STAT_TX_AR_ACK	= 0x40,
	I2C_STAT_TX_AR_NAK	= 0x48,
	I2C_STAT_RXD_ACK	= 0x50,
	I2C_STAT_RXD_NAK	= 0x58,
	I2C_STAT_IDLE		= 0xf8,
};

enum {
	/* Acknowledge bit */
	I2C_CONTROL_ACK	    = 0x00000004,
	/* Interrupt flag */
	I2C_CONTROL_IFLG	= 0x00000008,
	/* Stop bit */
	I2C_CONTROL_STOP	= 0x00000010,
	/* Start bit */
	I2C_CONTROL_START	= 0x00000020,
	/* I2C enable */
	I2C_CONTROL_TWSIEN	= 0x00000040,
	/* Interrupt enable */
	I2C_CONTROL_INTEN	= 0x00000080,
};

enum  {
	/* Send NAK after received byte */
	I2C_READ_NAK = 0,
	/* Send ACK after received byte */
	I2C_READ_ACK = 1,
};

void sys_i2c_init(void);
int sys_i2c_read(uint8_t addr, uint8_t *s_data, int s_len, uint8_t *r_data, int r_len);
int sys_i2c_write(uint8_t addr, uint8_t *m_data, int m_len, uint8_t *s_data, int s_len);

#endif