#ifndef __SYS_IRQ_H__
#define __SYS_IRQ_H__

#include "io.h"

#define SYS_IRQ_TIMER0			(80)
#define SYS_IRQ_TIMER1			(81)

#define SYS_IRQ_END             (400)


typedef void (*sys_irq_handler_t)(void);

void sys_irq_register_handler(uint32_t irq, sys_irq_handler_t handler);
void sys_irq_register_gpio_handler(uint32_t irq, sys_irq_handler_t handler);
void gic400_irq_enable(int irq);
void gic400_irq_disable(int irq);
void gic400_set_cpu(int irq, int cpumask);
void gic400_dist_init(void);
void gic400_cpu_init(void);
void gic400_set_priority(int irq, int priority);
void gic400_send_sgi(int irq, int cpumask);
uint32_t gic400_get_irq_num(void);
void gic400_irq_end(uint32_t irq_num);

#endif 
