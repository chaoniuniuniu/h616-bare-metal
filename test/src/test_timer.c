#include "drive_timer.h"
#include "sys_irq.h"
#include "printk.h"

static void timer0_handle(void)
{
	static uint32_t time_cnt = 0;

	time_cnt++;
	if(time_cnt >= 1000)
	{
		time_cnt = 0;
		
		printk("time 0\r\n");
	}

	drive_timer_irq_clear_pend(TIMER_0);   
}

void test_timer0_init(void)
{
    gic400_set_priority(SYS_IRQ_TIMER0, 0xb0); 

	/* 关闭timer */
	drive_timer_disable(TIMER_0);
	drive_timer_irq_disable(TIMER_0);       

	/* 设置定时器时钟 */
	drive_timer_set_clock(TIMER_0, TIMER_CLOCK_OSC24M, TIMER_CLK_PRE_8);
	/* 设置定时器模式 */
	drive_timer_set_mode(TIMER_0, TIMER_MODE_CONTINUOUS);
	/* 设置定时器重装载值 */
	drive_timer_set_interval(TIMER_0, 3000);
	
	/* 设置timer中断 */
	gic400_irq_enable(SYS_IRQ_TIMER0);
	sys_irq_register_handler(SYS_IRQ_TIMER0, timer0_handle);
	
	/* 使能timer */
	drive_timer_enable(TIMER_0);
	drive_timer_irq_enable(TIMER_0); 

}