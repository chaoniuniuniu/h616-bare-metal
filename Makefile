CROSS_COMPILE = aarch64-linux-gnu-
NAME = test
BUILD = build

MKSUNXI = ./mksunxi

CC = $(CROSS_COMPILE)gcc
LD = $(CROSS_COMPILE)ld
OBJCOPY = $(CROSS_COMPILE)objcopy
OBJDUMP = $(CROSS_COMPILE)objdump
SZ = $(CROSS_COMPILE)size

LIBS = 
CFLAGS = -Wall -Os -fno-strict-aliasing -fno-stack-protector
LDFLAGS	= -T h616.ld -nostdlib
MCFLAGS = -march=armv8-a -mcpu=cortex-a53 -mtune=cortex-a53 -mstrict-align


C_SOURCES = \
main.c \
$(wildcard system/src/*.c) \
$(wildcard arch/src/*.c) \
$(wildcard armv8/*.c) \
$(wildcard driver/src/*.c) \
$(wildcard test/src/*.c) \

ASM_SOURCES = \
$(wildcard armv8/*.s) \
start.s \


INCLUDES = \
-Isystem/inc \
-Iarch/inc \
-Iarmv8 \
-Idriver/inc \
-Itest/inc \

OBJS = $(addprefix $(BUILD)/,$(notdir $(C_SOURCES:.c=.o)))
vpath %.c $(sort $(dir $(C_SOURCES)))

OBJS += $(addprefix $(BUILD)/,$(notdir $(ASM_SOURCES:.s=.o)))
vpath %.s $(sort $(dir $(ASM_SOURCES)))

$(BUILD)/$(NAME).bin: $(BUILD)/$(NAME).elf | $(BUILD) 
	$(OBJCOPY) -O binary $^ $@
#	@$(MKSUNXI) $@

$(BUILD)/$(NAME).elf: $(OBJS) | $(BUILD) 
	$(CC) $(LDFLAGS) -Wl,--cref,-Map=$@.map -o $@ $^ $(LIBS)
	$(SZ) $@

$(BUILD)/%.o: %.s | $(BUILD) 
	$(CC) $(MCFLAGS) $(CFLAGS) -c $(INCLUDES) -o $@ $<

$(BUILD)/%.o: %.c | $(BUILD) 
	$(CC) $(MCFLAGS) $(CFLAGS) -c $(INCLUDES) -o $@ $<

$(BUILD):
	mkdir $@	

clean:
	rm -rf $(BUILD)

write:
	sudo sunxi-fel write 0x40000000 $(BUILD)/$(NAME).bin; sudo sunxi-fel exe 0x40000000

flash:
	sudo sunxi-fel spiflash-write 0x100000 $(BUILD)/$(NAME).bin

dis:
	$(OBJDUMP) -D $(BUILD)/$(NAME).elf > $(BUILD)/$(NAME).dis