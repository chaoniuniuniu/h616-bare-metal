/*******************************************************************************
  * @file   	drive_timer.c
  * @date    	
  * @brief  
  * @author			
********************************************************************************/

/*******************************************************************************
 * head files
********************************************************************************/
#include "drive_timer.h"
#include "sys_irq.h"

/*******************************************************************************
 * define
********************************************************************************/
#define TIMER_IRQ_EN	(0x00)
#define TIMER_IRQ_STA	(0x04)
#define TIMER_CTRL(x)	((x + 1) * 0x10 + 0x00)
#define TIMER_INTV(x)	((x + 1) * 0x10 + 0x04)
#define TIMER_CUR(x)	((x + 1) * 0x10 + 0x08)

#define TIMER_BASE		(0x03009000)

/*******************************************************************************
  * @brief     
  * @note     
  * @param
  * @retval     
  * @date       22/09/21    new
********************************************************************************/
void drive_timer_enable(timer_ch_t ntimer)
{
	uint32_t val;
	
	val = read32(TIMER_BASE + TIMER_CTRL(ntimer));
	val |= (1 << 0);
	write32(TIMER_BASE + TIMER_CTRL(ntimer), val);	
}

/*******************************************************************************
  * @brief     
  * @note     
  * @param
  * @retval     
  * @date       22/09/21    new
********************************************************************************/
void drive_timer_disable(timer_ch_t ntimer)
{
	uint32_t val;
	
	val = read32(TIMER_BASE + TIMER_CTRL(ntimer));
	val &= ~(1 << 0);
	write32(TIMER_BASE + TIMER_CTRL(ntimer), val);	
}

/*******************************************************************************
  * @brief     
  * @note     
  * @param
  * @retval     
  * @date       22/09/21    new
********************************************************************************/
void drive_timer_set_clock(timer_ch_t ntimer, timer_clk_src_t clk_src, timer_clk_pre_t clk_pre)
{
	uint32_t val;
	
	val = read32(TIMER_BASE + TIMER_CTRL(ntimer));
	val &= ~(3 << 2);
	val |= (clk_src << 2);
	val &= ~(7 << 4);
	val |= (clk_pre << 4);
	write32(TIMER_BASE + TIMER_CTRL(ntimer), val);	
}

/*******************************************************************************
  * @brief     
  * @note     
  * @param
  * @retval     
  * @date       22/09/21    new
********************************************************************************/
void drive_timer_set_mode(timer_ch_t ntimer, timer_mode_t mode)
{
	uint32_t val;
	
	val = read32(TIMER_BASE + TIMER_CTRL(ntimer));
    val &= ~(1 << 7);
	val |= (mode << 7);
	write32(TIMER_BASE + TIMER_CTRL(ntimer), val);		
}

/*******************************************************************************
  * @brief     
  * @note     
  * @param
  * @retval     
  * @date       22/09/21    new
********************************************************************************/
void drive_timer_reload(timer_ch_t ntimer)
{
	uint32_t val;
	
	val = read32(TIMER_BASE + TIMER_CTRL(ntimer));
	val |= (1 << 1);
	write32(TIMER_BASE + TIMER_CTRL(ntimer), val);		

	while(read32(TIMER_BASE + TIMER_CTRL(ntimer)) & (1 << 1));
}

/*******************************************************************************
  * @brief     
  * @note     
  * @param
  * @retval     
  * @date       22/09/21    new
********************************************************************************/
void drive_timer_set_interval(timer_ch_t ntimer, uint32_t val)
{
	write32(TIMER_BASE + TIMER_INTV(ntimer), val);	
}

/*******************************************************************************
  * @brief     
  * @note     
  * @param
  * @retval     
  * @date       22/09/21    new
********************************************************************************/
void drive_timer_set_current(timer_ch_t ntimer, uint32_t val)
{
	write32(TIMER_BASE + TIMER_CUR(ntimer), val);	
}

/*******************************************************************************
  * @brief     
  * @note     
  * @param
  * @retval     
  * @date       22/09/21    new
********************************************************************************/
void drive_timer_irq_enable(timer_ch_t ntimer)
{	
	uint32_t val;
	
	val = read32(TIMER_BASE + TIMER_IRQ_EN);
	val |= (1 << ntimer);
	write32(TIMER_BASE + TIMER_IRQ_EN, val);
}

/*******************************************************************************
  * @brief     
  * @note     
  * @param
  * @retval     
  * @date       22/09/21    new
********************************************************************************/
void drive_timer_irq_disable(timer_ch_t ntimer)
{	
	uint32_t val;
	
	val = read32(TIMER_BASE + TIMER_IRQ_EN);
	val &= ~(1 << ntimer);
	write32(TIMER_BASE + TIMER_IRQ_EN, val);
}

/*******************************************************************************
  * @brief     
  * @note     
  * @param
  * @retval     
  * @date       22/09/21    new
********************************************************************************/
void drive_timer_irq_clear_pend(timer_ch_t ntimer)
{
	uint32_t val;
	
	val = read32(TIMER_BASE + TIMER_IRQ_STA);
	val |= (1 << ntimer);
	write32(TIMER_BASE + TIMER_IRQ_STA, val);	
}

/*******************************************************************************
  * @brief     
  * @note     
  * @param
  * @retval     
  * @date       22/09/21    new
********************************************************************************/
uint8_t drive_timer_irq_read_pend(timer_ch_t ntimer)
{
	uint32_t val;
	
	val = read32(TIMER_BASE + TIMER_IRQ_STA);  
    if(val & (1 << ntimer))
    {
        return 1;
    }

    return 0;
}

/********************************END OF FILE************************************/
