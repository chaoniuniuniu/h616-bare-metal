/*******************************************************************************
  * @file    	drive_gpio.h
  * @date    	
  * @brief   
  * @author			
********************************************************************************/
#ifndef _DRIVE_TIMER_H
#define _DRIVE_TIMER_H

/*******************************************************************************
 * head files
********************************************************************************/
#include "io.h"

/*******************************************************************************
	enum
********************************************************************************/
typedef enum
{
	TIMER_MODE_CONTINUOUS = 0,
	TIMER_MODE_SINGLE,
} timer_mode_t;

typedef enum
{
    TIMER_0 = 0,
    TIMER_1,
} timer_ch_t;

typedef enum
{
	TIMER_CLOCK_LOSC = 0,
	TIMER_CLOCK_OSC24M,
} timer_clk_src_t;

typedef enum
{
	TIMER_CLK_PRE_1 = 0,
	TIMER_CLK_PRE_2,
	TIMER_CLK_PRE_4,
	TIMER_CLK_PRE_8,
	TIMER_CLK_PRE_16,
	TIMER_CLK_PRE_32,
	TIMER_CLK_PRE_64,
	TIMER_CLK_PRE_128,
} timer_clk_pre_t;

/*******************************************************************************
 * function
********************************************************************************/
void drive_timer_enable(timer_ch_t ntimer);
void drive_timer_disable(timer_ch_t ntimer);
void drive_timer_set_clock(timer_ch_t ntimer, timer_clk_src_t clk_src, timer_clk_pre_t clk_pre);
void drive_timer_set_mode(timer_ch_t ntimer, timer_mode_t mode);
void drive_timer_reload(timer_ch_t ntimer);
void drive_timer_set_interval(timer_ch_t ntimer, uint32_t val);
void drive_timer_set_current(timer_ch_t ntimer, uint32_t val);
void drive_timer_irq_enable(timer_ch_t ntimer);
void drive_timer_irq_disable(timer_ch_t ntimer);
void drive_timer_irq_clear_pend(timer_ch_t ntimer);
uint8_t drive_timer_irq_read_pend(timer_ch_t ntimer);

#endif
/********************************END OF FILE************************************/
