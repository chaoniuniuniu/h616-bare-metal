/* refer to Bare-metal Boot Code for ARMv8-A Processors */
    .global mmu_init

mmu_init:
    // Initialize translation table control registers
    LDR X1, =0x3520     // 4GB space 4KB granularity
                        // Inner-shareable.
    MSR TCR_EL3, X1     // Normal Inner and Outer Cacheable

    LDR X1, =0xFF440400 // ATTR0 Device-nGnRnE ATTR1 Device.
    MSR MAIR_EL3, X1    // ATTR2 Normal Non-Cacheable
                        // ATTR3 Normal Cacheable.

    ADR X0, ttb0_base   // ttb0_base must be a 4KB-aligned address.
    MSR TTBR0_EL3, X0

    // The first entry is 1GB block from 0x00000000 to 0x3FFFFFFF.
    LDR X2, =0x00000741         // Executable Inner and Outer Shareable.
    STR X2, [X0], #8            // R/W at all ELs secure memory
                                // AttrIdx=000 Device-nGnRnE.

    // The second entry is 1GB block from 0x40000000 to 0x7FFFFFFF.
    LDR X2, =0x4000074D         // Executable Inner and Outer Shareable.
    STR X2, [X0], #8            // R/W at all ELs secure memory
                                // AttrIdx=011 Normal Cacheable

    // It is implemented in the CPUECTLR register.
    MRS X0, S3_1_C15_C2_1
    ORR X0, X0, #(0x1 << 6)     // The SMP bit.
    MSR S3_1_C15_C2_1, X0

    // Enable caches and the MMU.
    MRS X0, SCTLR_EL3
    ORR X0, X0, #(0x1 << 2)     // The C bit (data cache).
    ORR X0, X0, #(0x1 << 12)    // The I bit (instruction cache).
    ORR X0, X0, #0x1            // The M bit (MMU).
    MSR SCTLR_EL3, X0
    DSB SY
    ISB

    RET


