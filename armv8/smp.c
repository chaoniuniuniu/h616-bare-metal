#include "io.h"

#define SUNXI_CPUCFG_BASE		0x09010000
#define SUNXI_R_CPUCFG_BASE		0x07000400

/* c = cluster, n = core */
#define SUNXI_CPUCFG_CLS_CTRL_REG0(c)	(SUNXI_CPUCFG_BASE + 0x0010 + (c) * 0x10)
#define SUNXI_CPUCFG_CLS_CTRL_REG1(c)	(SUNXI_CPUCFG_BASE + 0x0014 + (c) * 0x10)
#define SUNXI_CPUCFG_CACHE_CFG_REG	    (SUNXI_CPUCFG_BASE + 0x0024)
#define SUNXI_CPUCFG_DBG_REG0		    (SUNXI_CPUCFG_BASE + 0x00c0)

#define SUNXI_CPUCFG_RST_CTRL_REG(c)	(SUNXI_CPUCFG_BASE + 0x0000 + (c) * 4)
#define SUNXI_CPUCFG_RVBAR_LO_REG(n)	(SUNXI_CPUCFG_BASE + 0x0040 + (n) * 8)
#define SUNXI_CPUCFG_RVBAR_HI_REG(n)	(SUNXI_CPUCFG_BASE + 0x0044 + (n) * 8)

#define SUNXI_POWERON_RST_REG(c)	    (SUNXI_R_CPUCFG_BASE + 0x0040 + (c) * 4)
#define SUNXI_POWEROFF_GATING_REG(c)	(SUNXI_R_CPUCFG_BASE + 0x0044 + (c) * 4)
#define SUNXI_CPU_POWER_CLAMP_REG(c, n)	(SUNXI_R_CPUCFG_BASE + 0x0050 + (c) * 0x10 + (n) * 4)

#define SUNXI_CPUIDLE_EN_REG		    (SUNXI_R_CPUCFG_BASE + 0x0100)
#define SUNXI_CORE_CLOSE_REG		    (SUNXI_R_CPUCFG_BASE + 0x0104)
#define SUNXI_PWR_SW_DELAY_REG		    (SUNXI_R_CPUCFG_BASE + 0x0140)
#define SUNXI_CONFIG_DELAY_REG		    (SUNXI_R_CPUCFG_BASE + 0x0144)

#define SUNXI_AA64nAA32_REG		        SUNXI_CPUCFG_CLS_CTRL_REG0
#define SUNXI_AA64nAA32_OFFSET		    24

#define RVBARADDR_L(n)                  (SUNXI_CPUCFG_BASE + 0x0040 + (n) * 8)
#define RVBARADDR_H(n)                  (SUNXI_CPUCFG_BASE + 0x0044 + (n) * 8)


static void sunxi_cpu_enable_power(unsigned int cluster, unsigned int core)
{
	if (read32(SUNXI_CPU_POWER_CLAMP_REG(cluster, core)) == 0)
		return;

	/* Power enable sequence from original Allwinner sources */
	write32(SUNXI_CPU_POWER_CLAMP_REG(cluster, core), 0xfe);
	write32(SUNXI_CPU_POWER_CLAMP_REG(cluster, core), 0xf8);
	write32(SUNXI_CPU_POWER_CLAMP_REG(cluster, core), 0xe0);
	write32(SUNXI_CPU_POWER_CLAMP_REG(cluster, core), 0x80);
	write32(SUNXI_CPU_POWER_CLAMP_REG(cluster, core), 0x00);
}

static void sunxi_cpu_on(unsigned int cluster, unsigned int core)
{
	/* Assert CPU core reset */
	clrbits_le32(SUNXI_CPUCFG_RST_CTRL_REG(cluster), BIT(core));
	/* Assert CPU power-on reset */
	clrbits_le32(SUNXI_POWERON_RST_REG(cluster), BIT(core));
	/* Set CPU to start in AArch64 mode */
	setbits_le32(SUNXI_AA64nAA32_REG(cluster), BIT(SUNXI_AA64nAA32_OFFSET + core));
	/* Apply power to the CPU */
	sunxi_cpu_enable_power(cluster, core);
	/* Release the core output clamps */
	clrbits_le32(SUNXI_POWEROFF_GATING_REG(cluster), BIT(core));
	/* Deassert CPU power-on reset */
	setbits_le32(SUNXI_POWERON_RST_REG(cluster), BIT(core));
	/* Deassert CPU core reset */
	setbits_le32(SUNXI_CPUCFG_RST_CTRL_REG(cluster), BIT(core));

	/* Assert DBGPWRDUP */
	setbits_le32(SUNXI_CPUCFG_DBG_REG0, BIT(core));
}

void sunxi_cpu_start(unsigned int core, void *func) 
{
    uint64_t temp = (uint64_t)func;
    write32(RVBARADDR_L(core), temp & 0xffffffff);
    write32(RVBARADDR_H(core), temp >> 32);

    sunxi_cpu_on(0, core);
}

