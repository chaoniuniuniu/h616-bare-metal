
#ifndef _FEL_UTILS_H
#define _FEL_UTILS_H

#include "io.h"

struct fel_stash {
	unsigned int sp;
	unsigned int lr;
	unsigned int cpsr;
	unsigned int sctlr;
	unsigned int vbar;
	unsigned int cr;
};

/* return_to_fel() - Return to BROM from SPL
 *
 * This returns back into the BROM after U-Boot SPL has performed its initial
 * init. It uses the provided lr and sp to do so.
 *
 * @lr:		BROM link register value (return address)
 * @sp:		BROM stack pointer
 */
void return_to_fel(uint32_t lr, uint32_t sp);

#endif
