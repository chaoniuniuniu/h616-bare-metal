
/*
 * Branch according to exception level
 */
.macro	switch_el, xreg, el3_label, el2_label, el1_label
	mrs	\xreg, CurrentEL
	cmp	\xreg, #0x8
	b.gt	\el3_label
	b.eq	\el2_label
	b.lt	\el1_label
.endm

.global _start
_start:
	/* Boot head information for BROM */
	.long 0xea000016
	.long 0x12345678 
    .long __code_size 
    .long 0x40000000
	.long 0, 0, 0, 0
	.long 0, 0, 0, 0, 0, 0, 0, 0
	.long 0, 0, 0, 0, 0, 0, 0, 0

/*
 * Switch into AArch64 if needed.
 */
	tst     x0, x0      // this is "b #0x84" in ARM
	b       reset
	.space  0x7c

	.word	0xe28f0058	// add     r0, pc, #88
	.word	0xe59f1054	// ldr     r1, [pc, #84]
	.word	0xe0800001	// add     r0, r0, r1
	.word	0xe580d000	// str     sp, [r0]
	.word	0xe580e004	// str     lr, [r0, #4]
	.word	0xe10fe000	// mrs     lr, CPSR
	.word	0xe580e008	// str     lr, [r0, #8]
	.word	0xee11ef10	// mrc     15, 0, lr, cr1, cr0, {0}
	.word	0xe580e00c	// str     lr, [r0, #12]
	.word	0xee1cef10	// mrc     15, 0, lr, cr12, cr0, {0}
	.word	0xe580e010	// str     lr, [r0, #16]

	.word	0xe59f1024	// ldr     r1, [pc, #36] ; 0x170000a0
	.word	0xe59f0024	// ldr     r0, [pc, #36] ; CONFIG_*_TEXT_BASE
	.word	0xe5810000	// str     r0, [r1]
	.word	0xf57ff04f	// dsb     sy
	.word	0xf57ff06f	// isb     sy
	.word	0xee1c0f50	// mrc     15, 0, r0, cr12, cr0, {2} ; RMR
	.word	0xe3800003	// orr     r0, r0, #3
	.word	0xee0c0f50	// mcr     15, 0, r0, cr12, cr0, {2} ; RMR
	.word	0xf57ff06f	// isb     sy
	.word	0xe320f003	// wfi
	.word	0xeafffffd	// b       @wfi

	.word	0x09010040	// writeable RVBAR mapping address
	.word	0x40000060  //spl base addr
	.word	fel_stash - .

reset:
    /* 默认进入 el3 */
    adr	x0, vectors
    msr	vbar_el3, x0 
	mrs	x0, scr_el3
	orr	x0, x0, #0xf	    /* SCR_EL3.NS|IRQ|FIQ|EA */
	msr	scr_el3, x0
    msr	cptr_el3, xzr		/* Enable FP/SIMD */
    msr	daifclr, #0x4		/* Unmask SError interrupts */

	ldr	x0, =24000000
	msr	cntfrq_el0, x0		/* Initialize CNTFRQ */

	//bl sys_uart_init
    //bl sys_clock_init
    //bl sys_ddr_init
    
    /* clear bss */
	ldr	x0, =__bss_start		
	ldr	x1, =__bss_end		
clear_loop:
	str	xzr, [x0], #8
	cmp	x0, x1
	b.lo	clear_loop

    
	ldr	x0, =0x60000000
	bic	sp, x0, #0xf	    /* 16-byte alignment for ABI compliance */

    bl mmu_init

    bl gic400_init

    msr	daifclr, #0x2       /* enable irq */

    bl main

    b .

/*
    ldr w0, =0x0300B04C
    ldr w1, =0x77177777
    str w1,[x0]

    ldr w0, =0x0300B058
    ldr w1, =(1 << 13)
    str w1,[x0]  
*/